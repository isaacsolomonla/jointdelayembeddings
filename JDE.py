import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import MDS
import os
import random
from mpl_toolkits.mplot3d import Axes3D
import math
from ripser import ripser
from persim import plot_diagrams
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

def get_person_from_file(file,n):
    """
    Gets motionsense time series for an individual doing an activity.
    :param file: The file corresponing to an activity.
    :param n: The index of the individual.
    :return: An array whose rows correspond to distinct time series.
    """
    subs= [i for i in os.listdir(file) if i.endswith('csv')]
    sub_file = np.loadtxt(file + '/' + subs[n], delimiter = ',',skiprows = 1, usecols = range(1,13)) 
    return sub_file

    
def visualize_MDS(E,colors):
    """
    Applies 3d MDS transform to an embedding then plots it in 3d.
    :param E: The embedding matrix.
    :param colors: The color values for the plot.
    """
    embedding = MDS(n_components=3)
    E_t = embedding.fit_transform(E)
    visualize_embedding(E_t,colors,10,50)
    
def visualize_embedding(E,colors,elev=10,azim=50):
    """
    Plots the first 3 dimension of an embedding in 3d.
    :param E: The embedding matrix.
    :param elev: elevation parameter for initial 3d view.
    :param azim: azimuth parameter for initial 3d view.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(elev=elev, azim=azim)    
    ax.scatter(E[:,0],E[:,1],E[:,2],s=20,color=colors,alpha=0.6)
    
def random_normalized_directions(n,k): 
    """
    Samples n vectors uniformly on the (k-1)-sphere
    :param n: Number of vectors.
    :param k: Ambien dimension of the vectors/sphere.
    :return: An nxk array whose rows are the vectors.
    """
    directions =  np.random.randn(n,k)
    # Normalize directions
    for i in range(n):
        directions[i,:] /= np.linalg.norm(directions[i,:])
    return directions


def f1(x,R,r,a,b,x0,y0):
    return (R + r * np.cos(a*x + x0 ))*np.cos(b*x + y0)
def f2(x,R,r,a,b,x0,y0):
    return (R + r * np.cos(a*x + x0 ))*np.sin(b*x + y0)
def f3(x,R,r,a,b,x0,y0):
    return r*np.sin(a*x + x0)

def generate_torus_curve(N,R,r,a,b,x0,y0):
    """
    Generates a torus curve data set.
    :param N: Number of points on the curve.
    :param R: Outer radius of torus.
    :param r: Inner radius of torus.
    :param a: First slope parameter.
    :param b: Second slope parameter.
    :param x_0: Initial x coordinate in parameter space.
    :param y_0: Initial y coordinate in parameter space 
    :return: A curve on a torus.
    """
    return np.asarray([[f1(x,R,r,a,b,x0,y0),f2(x,R,r,a,b,x0,y0),f3(x,R,r,a,b,x0,y0)] for x in np.linspace(0,2*np.pi,N)])



def random_basepoints(P,x0,x1,y0,y1,z0,z1):
    """
    Generates random points, uniformly sampled in a cube.
    :param x_0: Left endpoint of x-interval.
    :param x_1: Right endpoint of x-interval.
    :param y_0: Left endpoint of y-interval.
    :param y_1: Right endpoint of y-interval.
    :param z_1: Left endpoint of z-interval.
    :param z_2: Right endpoint of z-interval.
    :return: P points in the cube [x0,x1] x [y0,y1] x [z0,z1]
    """
    basepoints = np.zeros((P,3))
    xs = np.random.uniform(x0,x1,P)
    ys = np.random.uniform(y0,y1,P)
    zs = np.random.uniform(z0,z1,P)
    basepoints[:,0] = xs
    basepoints[:,1] = ys
    basepoints[:,2] = zs
    return basepoints


def curve_dist_ts(curve,basepoint):
    """
    Maps a curve to 1d using distances from a fixed basepoint.
    :param curve: The curve to be mapped.
    :param basepoint: The point defining the mapping.
    :return: A 1d time series.
    """
    a = len(curve)
    y = []
    for i in range(a):
        y.append(np.linalg.norm(curve[i] - basepoint))
    return(y)


def curve_proj_ts(curve,direction):
    """
    Projects a curve to 1d by taking dot products with a fixed direction vector.
    :param curve: The curve to be projectde.
    :param direction: The direction vector defining the projection.
    :return: A 1d time series.
    """
    a = len(curve)
    y = []
    for i in range(a):
        y.append(np.dot(direction,curve[i]))
    return(y)


    
    
class time_series_fusion:
    def __init__(self,d=10,lam=1,beta=0.5,kappa=0.1,n_iter=10):
        """
        Creates a time_series_fusion object
        :param d: The default dimension for takens-based methods.
        :param lam: The default lambda parameter in the Gram-Schmidt norm scheme.
        :param beta: The default beta parameter for turning distance matrices into SSMs.
        :param kappa: The default kappa parameter for turning distance matrices into SSMs.
        :param n_iter: The default number of iterations to run the SNF algorithm.
        :return: A time_series_fusion object.
        """
        self.d = d
        self.lam = lam
        self.beta = beta
        self.kappa = kappa
        self.n_iter = n_iter
        
    def embed_to_dist(self,T):
        """
        Transforms an embedded curve in R^k to a Euclidean distance matrix.
        :param T: The time series to transform.
        :return: Euclidean distance matrix.
        """
        N = len(T)
        D = np.zeros((N,N))
        for i in range(N):
            for j in range(i,N):
                D[i,j] = np.linalg.norm(T[i] - T[j])
                D[j,i] = D[i,j]
        return(D)
    
    
    def _GS(self,X):
        """
        Gram-Schmidt norm scheme.
        :param X: A collection of vectors to take the norm of.
        :return: The norm of the set of vectors, using the class's lambda.
        """
        lam = self.lam
        S = []
        Y = np.copy(X)
        (P,N) = np.shape(Y)
        for i in range(P):
            norms = [np.linalg.norm(Y[i,:]) for i in range(P)]
            idx = np.argmax(norms)
            v = np.copy(Y[idx,:])
            S.append(v)
            Y[idx:] = Y[idx:] - v
            for j in range(P):
                if norms[idx]>0.0000000000001:
                    dot = np.dot(Y[j,:],v)/(norms[idx]**2)
                    Y[j,:] = Y[j,:] - dot * lam * v
        return(np.linalg.norm(np.asarray(S)))
    
    def _embed_to_GS(self,T):
        """
        Transforms an embedded curve into a distance matrix using the Gram-Schmidt scheme.
        :param T: The embedded curve.
        :return: A distance matrix.
        """
        N = len(T)
        D = np.zeros((N,N))
        for i in range(N):
            for j in range(i,N):
                D[i,j] = self._GS(T[i] - T[j])
                D[j,i] = D[i,j]
        return(D)
    
    def _takens(self,T):
        """
        Performs a takens embedding on a time series in R^k, with delay=1, and dimension self.d.
        :param T: The time series to transform.
        :return: A time series in R^(k x d).
        """
        Z = np.asarray(T)
        d = self.d
        (P,N) = np.shape(T)
        taken_curve = []
        for i in range(N-d+1):
            taken_curve.append(Z[:,i:i+d])
        return np.asarray(taken_curve)
    
    def _nearest_neighbor_distances(self,l):
        """
        Computes the kappa fraction of smallest values in a list of distances.
        :param l: The list of distances.
        :return: A list of length ceil(kappa * length(l)).
        """
        kappa = self.kappa
        d = sorted(l)
        n = len(d)
        return(d[:math.ceil(kappa * n)])
    
    def _nearest_neighbor_cutoff(self,l):   
        """
        Computes the ceil(kappa*len(l)) smallest value in a list of distances.
        :param l: The list of distances.
        :return: The ceil(kappa*len(l)) entry in sorted(l).
        """
        kappa = self.kappa
        d = sorted(l)
        n = len(d)
        return(d[math.ceil(kappa * n)])    

    def _sigma(self,di,dj,dij):
        """
        Computes the quantity Sigma_ij from the paper.
        :param di: The vector of distances from xi.
        :param dj: The vector of distances from xj.
        :param dij: The distance from xi to xj.
        :return: The quantity Sigma_ij, measuring the average distances from xi and xj to nearby points.
        """
        kappa = self.kappa
        beta = self.beta
        S = 0
        n = len(di)
        Ni = self._nearest_neighbor_distances(di)
        Nj = self._nearest_neighbor_distances(dj)
        S+= (1.0/ (kappa * n)) * sum(Ni)
        S+= (1.0/ (kappa * n)) * sum(Nj)
        S+= dij
        S*= beta/3
        return S

    def _sigma_matrix(self,X):
        """
        Computes the full Sigma matrix used in the SNF pipeline.
        :param X: The matrix of distances used to compute Sigma.
        :return: The normalization matrix Sigma.
        """
        (n,m) = np.shape(X)
        S = np.zeros((n,m))
        for i in range(n):
            for j in range(n): 
                S[i,j] = self._sigma(X[i],X[j],X[i,j])
                S[j,i] = S[i,j]
        return(S)

    def dist_to_ssm(self,X):
        """
        Converts a distance matrix to a SSM, as in the paper.
        :param X: The distance matrix to be transformed.
        :return: A self-similarity matrix.
        """
        (n,m) = np.shape(X)
        W = np.zeros((n,m))
        for i in range(n):
            for j in range(i+1):
                W[i,j] = np.exp( - 1* (X[i,j]**2)/ self._sigma(X[i],X[j],X[i,j]))
                W[j,i] = W[i,j]
        return W

    def _dist_to_ssm_array(self,mats):
        """
        Converts a list of distance matrices into SSMs.
        :param mats: A list of distance matrices.
        :return: A list of SSMs.
        """
        new_mats = []
        for m in mats:
            new_mats.append(self.dist_to_ssm(m))
        return new_mats

    def _W2P(self,W):
        """
        Converts a similarity matrix W to a normalized matrix P, for use in SNF.
        :param W: An SSM.
        :return: A normalized matrix P.
        """
        (n,m) = np.shape(W)
        P = np.zeros((n,m))
        for i in range(n):
            P[i,i] = 0.5
            for j in range(i):
                P[i,j] = W[i,j]/(2*(sum(W[i])-1))
                P[j,i] = P[i,j]
        return P

    def _W2P_array(self,mats):
        """
        Converts a list of similarity matrices W to their normalized P matrices.
        :param mats: A list of SSMs.
        :return: A list of normalized matrices.
        """
        new_mats = []
        for m in mats:
            new_mats.append(self._W2P(m))
        return new_mats    

    def _W2S(self,W):
        """
        Converts a similarity matrix W to a normalized matrix S, for use in SNF.
        :param W: An SSM.
        :return: A normalized matrix S.
        """
        (n,m) = np.shape(W)
        S = np.zeros((n,m))
        for i in range(n):
            S[i,i] = 0.5
            cutoff = self._nearest_neighbor_cutoff(W[i])
            nbhd_sum = sum(self._nearest_neighbor_distances(W[i]))
            for j in range(i):
                if W[i,j] <= cutoff:
                    S[i,j] = W[i,j]/ (2*(nbhd_sum-1))
                    S[j,i] = S[i,j]
        return S

    def _W2S_array(self,mats):
        """
        Converts a list of similarity matrices W to their normalized S matrices.
        :param mats: A list of SSMs.
        :return: A list of normalized matrices.
        """
        new_mats = []
        for m in mats:
            new_mats.append(self._W2S(m))
        return new_mats    


    def _SNF_alternation(self,P_mats,S_mats):
        """
        The iterative step in the SNF algorithm.
        :param P_mats: The normalized P matrices of the SSMs.
        :param S_mats: The normalized S matrices of the SSMs.
        :return: An SNF-fused SSM.
        """
        m = len(P_mats)
        for i in range(m):
            P_sum = -1 * P_mats[i]
            for p in P_mats:
                P_sum += p
            P_sum *= (1.0/m)
            P_mats[i] = np.matmul(np.matmul(S_mats[i],P_sum),np.transpose(S_mats[i]))
        return P_mats

    def _iterate(self,fun,v_data,f_data):
        """
        A method that iterates another method on a piece of data.
        :param fun: The method to iterate.
        :param v_data: The input data to the method that is fed into the next iteration.
        :param f_data: The input data to the method that is fixed for all iterations.
        :return: The final output of the iteration.
        """
        n_iter = self.n_iter
        for i in range(n_iter):
            v_data = fun(v_data,f_data)
        return v_data
    
        

    def SNF(self,X):
        """
        Applies SNF to a list of time series by first converting them to distance matrices and then SSMs.
        :param X: A list of time series.
        :return: The SNF-fused SSM of the SSMs coming from the time series in X.
        """
        matrices = [self.embed_to_dist(T) for T in X]
        (N,N) = np.shape(matrices[0])
        m = len(matrices)
        W_array = [self.dist_to_ssm(L) for L in matrices]
        P_array = [self._W2P(W) for W in W_array]
        S_array = [self._W2S(W) for W in W_array]
        P_array = self._iterate(SNF_alternation,P_array,S_array)
        P_mat = np.zeros((N,N))
        for i in range(m):
            P_mat += P_array[i]
        P_mat = P_mat/m
        return(P_mat)
    
    def takens_JDL(self,X,d):
        """
        Applies Takens + JDL fusion to a set of P time series, thought of as a time series in R^P.
        :param X: A data matrix whose rows are different time series and whose columns correspond to time values.
        :param d: The dimension parameter in Takens embedding.
        :return: The Euclidean distance matrix obtained after applying a Takens embedding to X, which is a time series
                 in R^(p x d). 
        """
        self.d = d
        takens_embed = self._takens(X)
        return self.embed_to_dist(takens_embed)
    
    def takens_GS(self,X,d):
        """
        Applies Takens + Gram-Schmidt norm fusion to a set of P time series, thought of as a time series in R^P.
        :param X: A data matrix whose rows are different time series and whose columns correspond to time values.
        :param d: The dimension parameter in Takens embedding.
        :return: The Gram-Schmidt norm matrix obtained after applying a Takens embedding to X, which is a time series
                 in R^(p x d). 
        """
        self.d = d
        d = self.d
        lam = self.lam
        takens_embed = self._takens(X)
        return self._embed_to_GS(takens_embed)
    
    def JDL(self,matrices):
        """
        Applies JDL to a set of P distance matrices.
        :param matrices: A list of distance matrices.
        :param d: The dimension parameter in Takens embedding.
        :return: A fused distance matrix.
        """
        Z = np.zeros(matrices[0].shape)
        for m in matrices:
            Z+= np.multiply(m,m)
        return np.sqrt(Z)